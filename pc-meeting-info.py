#!/usr/local/bin/python
####
# pc-meeting-info.py
#
# This is a script intended to be helpful to members of the NANOG Program Committee (PC)
# and any future successors. It connects to the PC Tool API using the PC member's 
# username and password and downloads information about a given NANOG meeting, potentially
# including the materials uploaded by authors.
#
# Please note that there are several known issues with this tool, and a number of ways
# that I'm quite certain it will break. Please feel free to fix and/or improve this.
#
# I imagine two primary uses for this tool. First, if run without the -a flag it will
# download all of the materials for accepted talks, which may be helpful for reviewing
# presentation material prior to the conference. Second, if run with the -a flag then
# it will only download materials that have been marked as Approved in the PC Tool,
# which may be helpful for creating a folder structure containing presentation files
# that can be used on the presentation laptops during the conference.
#
# If run without the -a flag this tool will also generate files in each talk's
# folder called talk.json which includes the metadata that this script constructs
# about that talk. I'm not sure why that seems useful to me, but I imagine that
# the reviewer may want to double-check these details without having to connect
# to the PC Tool itself, e.g. during times when they may not have connectivity
# such as during the plane ride to a NANOG conference... Not that any self-respecting
# PC member would ever wait until the last minute to do reviews, of course... :)
#
# This tool also generates (with or without the -a flag) a file called agenda.csv
# which should more-or-less be able to be opened by a tool like Microsoft Excel. The
# contents of this file should be a list of all the talks that have been accepted
# the the PC, with shepherd and author details, etc. As an example, I use this to
# populate information in the shared meeting planning materials that the PC uses.
# (Specifically, as of the time of writing, the Google Sheet document with the 
# Presentation List tab... But I assume this will evolve over time.)
#
####
#
# Copyright 2018 Benson Schliesser
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
####

from __future__ import print_function
from __future__ import unicode_literals
import sys
import os
from datetime import datetime
from datetime import timezone
import pytz  # honestly, I should probably just use one TZ library...
import calendar
import time
import getopt
import json
import requests
import re
import csv
# some things I considered but gave up on... maybe later...
#import rest_framework
#from bs4 import BeautifulSoup

#
# configuration
#
time_bins_conf = './time_bins.conf'
api_base = 'https://pc.nanog.org/api/v1/'
meeting_path = 'meeting/'
proposal_path = 'talk/'
auth_url = 'https://pc.nanog.org/accounts/signin/'

#
# initializations
#
DEBUG = False
XDEBUG = False
opt_a = False
agenda = []

#
# let's get this party started
#
print("=== NANOG PC Material Extraction Tool ===")

# I apoligize, but I wanted to use some datetime features that didn't show up until 3.7
if sys.version_info < (3, 7):
    print("ERROR: Python 3.7 or later version required!")
    exit(1)

#
# process command line options
#
def usage():
    print("Usage: pc-meeting-info.py -u <USERNAME> -p <PASSWORD> -m <API MEETING ID> -f <LOCAL FOLDER> [-d] [-x] [-a]")
    print("\t<USERNAME> and <PASSWORD> are PC Tool account with appropriate permissions etc.")
    print("\tNote that <API MEETING ID> is the ID used by the API, not the canonical meeting number!")
    print("\t<LOCAL FOLDER> is the directory on the local machine for downloading materials files")
    print("\t-d  Debug (DEBUG) flag")
    print("\t-x  Extra Debug (XDEBUG) flag, implies -d")
    print("\t-a  Only download material with Approved status")
# I feel like there is probably a much more efficient way of doing this,
# but as an amateur python hacker I don't know what it is...
opt_status = {}
mandatory_opts = [ 'u', 'p', 'm', 'f']
for i in mandatory_opts:
    opt_status[i] = False
try:
    opts, _ = getopt.getopt(sys.argv[1:], "u:p:m:f:dxah")
except:
    print("Error with getopt:")
    usage()
    sys.exit(-1)
for opt, arg in opts:
    if opt in '-u':
        opt_u = str(arg)
        opt_status['u'] = True
    elif opt in '-p':
        opt_p = str(arg)
        opt_status['p'] = True
    elif opt in '-m':
        opt_m = str(arg)
        opt_status['m'] = True
    elif opt in '-f':
        opt_f = str(arg)
        opt_status['f'] = True
    elif opt in '-d':
        DEBUG = True
    elif opt in '-x':
        DEBUG = True
        XDEBUG = True
    elif opt in '-a':
        opt_a = True
    elif opt in '-h':
        usage()
        sys.exit(0)
    else:
        usage()
        sys.exit(-1)
for x in mandatory_opts:
    if not opt_status[x]:
        usage()
        sys.exit(-1)
if os.path.isdir(opt_f):
    folder = opt_f
else:
    print("ERROR: -f value " + opt_f + " is not a valid directory path")
    sys.exit(1)

#
# load the schedule details / time bins for sorting talks on each day into folders
#
if DEBUG: print("DEBUG: Loading time-bin config file " + time_bins_conf )
f = open(time_bins_conf, 'r')
tb_conf = json.load(f)
f.close()
if XDEBUG: print(repr(tb_conf))
tb = {}
for d in tb_conf:
    tb[d['day']] = []
    for b in d['bins']:
        t_start = time.strptime(b['start'], "%H:%M")
        t_stop = time.strptime(b['stop'], "%H:%M")
        t = (t_start, t_stop)
        tb[d['day']].append(t)
if DEBUG:
    print("DEBUG: Time Bins:")
    for d in tb.keys():
        print("DEBUG: \t\t" + d)
        for (t_start, t_stop) in tb[d]:
            print("DEBUG: \t\t\t" + time.strftime('%H:%M',t_start) + " - " + time.strftime('%H:%M',t_stop) + "" )

#
# create an authenticated session with the PC Tool server
#
s = requests.Session()
# session s will remember some cookies that are needed for CloudFlare
if DEBUG: print("DEBUG: Getting authentication form.")
r=s.get(auth_url)
if XDEBUG: print(r.text)
# this next step is a stupid hack to extract the CSRF token that's embedded in the login page...
pattern=re.compile("name='csrfmiddlewaretoken' value='(.+?)'")
mo = pattern.search(r.text)
if mo:
    if DEBUG: print("DEBUG: Extracted csrfmiddlewaretoken: " + mo.group(1))
    csrf = mo.group(1)
else:
    print("ERROR: csrfmiddlewaretoken RE pattern didn't match anything")
    exit(1)
# now authenticate with the username and pasword provided via command line
print("Authenticating with PC Tool...")
# note that CSRF protections require the Referer header as well as the csrfmiddlewaretoken in the form submission
r = s.post(auth_url, headers={'Referer': 'https://pc.nanog.org'}, data={'username': opt_u, 'password': opt_p, 'csrfmiddlewaretoken': csrf })
if XDEBUG: print(r.text)

#
# download the meeting info from the PC Tool API
#
url = api_base + meeting_path + opt_m + '/'
# If I was more clever and/or less lazy, I'd search the meeting list to translate the real meeting # into the database ID.
# But I'm not, so I didn't. Just pull https://pc.nanog.org/api/v1/meeting/ and find it yourself, unless you want to write
# the translation code...
if DEBUG: print("DEBUG: Pulling meeting info from " + url)
r = s.get(url)
if XDEBUG: print(r.text)
mtg = json.loads(r.text)
if XDEBUG: print(mtg)
mtg_number = str(mtg['number'])
mtg_loc = str(mtg['location'])
mtg_tz = str(mtg['time_zone'])
# go ahead and tell the person running the command that we've learned something
print("NANOG " + mtg_number + " in " + mtg_loc + " (" + mtg_tz + ")")
# the database stores timestamps in UTC, so we're going to need to translate later
# this creates the python object for the meeting timezone, using the string found above
tz = pytz.timezone(mtg_tz)

#
# create 'agenda' array of dicts that contain details about every accepted talk
#
for talk in mtg['talks']:
    if talk['status'] == 3:  # int(3) is the value for Accepted talks. I don't think these are available in the API, so I hard-coded
        talk_deets = {}
        talk_deets['title'] = talk['title']

        # Note that THIS WILL BREAK if the talk has not yet been scheduled...
        # TODO: do something intelligent with talk entries that have date_scheduled or duration set to null
        #
        # first we need to grab the timestamp from the PC Tool data
        #
        # the following doesn't work because timestamps in the PC Tool database have 'Z' appendix noting Zulu / UTC
        # i.e. they're stored as RFC 3339 but this mechanism wants "correct" ISO 8601
        #dt = datetime.fromisoformat(talk['date_scheduled']) 
        #
        # instead we parse the time directly, ignoring the Z and manually setting UTC
        x = datetime.strptime(talk['date_scheduled'], "%Y-%m-%dT%H:%M:%SZ").replace(tzinfo=timezone.utc)
        # then we convert it to the venue timezone using the tz object created above
        dt = x.astimezone(tz=tz)
        # then extract strings of the date, day of week, and time of the scheduled talk
        date = dt.date().isoformat()
        day = calendar.day_name[dt.date().weekday()]
        t_time = dt.time().isoformat()
        talk_deets['date'] = date
        talk_deets['day'] = day
        talk_deets['time'] = t_time
        if DEBUG: print("DEBUG: Talk at " + talk_deets['date'] + " " + talk_deets['time'] + " = " + talk_deets['title'] )

        # each talk entry in the meeting contains a link to the proposal details
        # but the format is /proposal/NUMBER/ whereas the API endpoint is actually /talk/NUMBER/
        pattern=re.compile("^/proposal/(.+?)$")
        mo = pattern.search(talk['url'])
        proposal_id = mo.group(1)
        if DEBUG: print("DEBUG: \tProposal ID: " + proposal_id)
        url = api_base + proposal_path + proposal_id + '/'

        # fetch the talk's proposal details
        r = s.get(url)
        prop_deets = json.loads(r.text)
        talk_deets['id'] = mtg_number + '.' + str(prop_deets['meeting_talkid'])
        talk_deets['can_record'] = bool(prop_deets['can_record'])
        # this is just a numeric value in the PC Tool database... we need some way to look-up string name
        # TODO: figure out how to translate room ID into a string name
        room = str(prop_deets['session'])
        talk_deets['room'] = room
        # create a list of shepherds for the talk
        shepherds = []
        for shepherd_link in prop_deets['shepherds']:
            r = s.get(shepherd_link)
            shepherd_deets = json.loads(r.text)
            shepherd = "" + shepherd_deets['first_name'] + " " + shepherd_deets['last_name'] + " <" + shepherd_deets['email'] + ">"
            shepherds.append(shepherd)
        talk_deets['shepherd'] = shepherds
        # create a list of speakers for the talk
        authors = []
        author_count = 0
        for author_link in prop_deets['authors']:
            author_count = author_count + 1
            r = s.get(author_link)
            author_deets = json.loads(r.text)
            author = "" + author_deets['first_name'] + " " + author_deets['last_name'] + " <" + author_deets['email'] + ">"
            authors.append(author)
        talk_deets['author'] = authors


        # create a list of the materials for the talk
        # (these should probably be refactored together some mythical day when I have spare time...)
        materials = []
        for material_link in prop_deets['material_set']:
            mi = {}
            r = s.get(material_link)
            material_deets = json.loads(r.text)
            mi['file'] = material_deets['media_url']
            x = datetime.strptime(material_deets['date_uploaded'], "%Y-%m-%dT%H:%M:%SZ").replace(tzinfo=timezone.utc)
            mi['timestamp'] = x.timestamp()
            mi['ver'] = material_deets['version_number']
            mi['approved'] = bool(material_deets['approved'])
            materials.append(mi)
        # make a list of any approved materials
        approved_mi = []
        for mi in materials:
            if mi['approved']:  # make a list of all approved materials
                approved_mi.append(mi)

        # decide which list of material to download later
        if opt_a:
            # if the -a option was used then we download the approved material
            talk_deets['material'] = approved_mi
        else:
            # otherwise we download all of the materials
            talk_deets['material'] = materials

        # finally, we ignore talks with 0 speakers, save the rest into the agenda array
        if DEBUG: print("DEBUG: \tAuthor Count: " + repr(author_count))
        if author_count > 0: # this is a hack to eliminate routine agenda items like lunch etc.
            agenda.append(talk_deets)
            if XDEBUG: print(talk_deets)
        elif DEBUG:
            print("DEBUG: \tNo Authors, Ignoring")

if XDEBUG: print(json.dumps(agenda, sort_keys=True, indent=4, separators=(',', ': ')))

#
# identify which time bin (folder) the talk is in
#
for talk in agenda:
    wd = talk['day']
    t_talk = time.strptime(talk['time'], "%H:%M:%S")
    for (t_start, t_stop) in tb[wd]:
        if t_stop > t_talk >= t_start:
            talk['time_bin'] = time.strftime("%H%M", t_start) + '-' + time.strftime("%H%M", t_stop)
            if DEBUG: print("DEBUG: Sorted " + talk['id'] + " into " + wd + " " + talk['time_bin'])
    if not talk.get('time_bin', False):
        print("ERROR: Talk " + talk['id'] + " could not be sorted into a session bin!")
        print(talk)
        exit(1)

#
# create a CSV file of the agenda
#
filename = folder + '/agenda.csv'
if DEBUG: print("DEBUG: creating agenda spreadsheet in " + filename)
csvfile = open(filename, 'w', newline='')
cw = csv.writer(csvfile, dialect='excel')
a_csv = []
a_header = ['Day','Time','Room','ID','Can Record','Title','Author','Shepherd','Slides Approved']
cw.writerow(a_header)
for talk in agenda:
    row = []
    row.append(talk['day'])
    row.append(talk['time'])
    row.append(talk['room'])
    row.append(talk['id'])
    a = ''
    if bool(talk['can_record']):
        a = 'Yes'
    else:
        a = 'No'
    row.append(a)
    row.append(talk['title'])
    x = a = ''
    for x in talk['author']:
        if not a == '': a = a + '\n'
        a = a + x
    row.append(a)
    x = a = ''
    for x in talk['shepherd']:
        if not a == '': a = a + '\n'
        a = a + x
    row.append(a)
    x = ''
    a = 'No'
    for x in talk['material']:
        if bool(x['approved']):
            a = 'Yes'
    row.append(a)
    cw.writerow(row)
csvfile.close()

#
# create folder structure and download materials
#
for talk in agenda:
    if DEBUG: print("DEBUG: Starting download process for " + talk['id'])
    wd = talk['day']
    # TODO: make it possible to download approved material into main bin directory + unapproved material into subfolders
    if opt_a:
        # if we are downloading accepted materials then they go directly in to the time_bin folder
        directory = os.path.normpath( folder + '/' + wd + '/' + talk['time_bin'] )
        # the room / session value is actually just a pain in the ass to use...
        #directory = os.path.normpath( folder + '/' + str(talk['room']) + '/' + wd + '/' + talk['time_bin'] )
    else:
        # if we are downloading all materials then we'll put them in ./unapproved/ sub-folders named for the talk ID
        directory = os.path.normpath( folder + '/' + str(talk['room']) + '/' + wd + '/' + talk['time_bin'] + '/unapproved/' + talk['id'] ) 
    if DEBUG: print("DEBUG: working in directory " + directory)
    if not os.path.exists(directory):
        if DEBUG: print("DEBUG: directory does not exist, creating...")
        os.makedirs(directory)
    elif DEBUG:
        # TODO: add command line option to avoid overwriting existing directories, files, etc.
        print("DEBUG: directory " + directory + " already exists but proceeding anyway!")
    # if we are downloading all materials then create a json file with talk metadata
    # I don't know why I'm doing this... maybe for future-proofing, maybe just for correlation during review process
    if not opt_a:
        filename = directory + '/talk.json'
        if os.path.isfile(filename):
            if DEBUG: print("DEBUG: file " + filename + " already exists and will be overwritten!")
        f = open(filename, 'w')
        f.write(json.dumps(talk, sort_keys=True, indent=4, separators=(',', ': ')))
    # download actual material files
    for material in talk['material']:
        fn = material['file'].split('/')[-1]
        x = talk['time'].split(':')
        ft = ''
        ft = ft.join([x[0],x[1]])
        if opt_a:
            filename = directory + '/' + ft + '_' + fn
        else:
            filename = directory + '/' + fn
        if DEBUG: print("DEBUG: downloading " + material['file'] + " and storing it as " + filename + " ...")
        f = open(filename, 'xb')
        r = s.get(material['file'])
        f.write(r.content)  # this is probably not the most efficient way to do this...
        f.close()

