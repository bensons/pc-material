#!/usr/local/bin/python
####
# create-time-bins.py
#
# This is a simple script to generate example json to be used as time_bins.conf
# with the pc-meeting-info.py tool. This is a pretty silly tool, and the only reason
# I'm providing this is so that I have a place to document the following:
#
# This script assumes the same range of time will be used on each day, but the
# pc-meeting-info.py script doesn't make any such assumption as of the time of writing.
# As far as I'm aware, the resulting json may be manipulated to create an arbitrary
# quantity of time ranges with arbitrary range values on each day as long as they don't 
# overlap. The scripts do not support multiple instances of the same weekdays, in other
# words meaning that I assumed the entire conference would last less than a week.
# 
# Resulting timestamps are imprecise, but e.g. may be read like this:
#        t_start = time.strptime(a, "%H:%M")
#        t_stop = time.strptime(z, "%H:%M")
#
####
#
# Copyright 2018 Benson Schliesser
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
####

import json

days = ['Monday', 'Tuesday', 'Wednesday']
bins = [ ('08:00', '12:00'), ('13:30', '15:30'), ('16:00', '18:00')  ]

x = []
for d in days:
    n = {}
    n['day'] = d
    n['bins'] = []
    for (a, z) in bins:
        t = {'start': a, 'stop': z}
        n['bins'].append(t)
    x.append(n)

print(json.dumps(x, sort_keys=False, indent=4))


